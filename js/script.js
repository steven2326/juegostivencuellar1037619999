/**
 * Created by SCuellar on 28/02/2016.
 */
'use strict';

var ENUM_PREGUNTAS = {
    1 : '¿Cual es la capital de Rusia?',
    2 : '¿Cual es la capital de Japon?',
    3 : '¿En que lugar de Europa se encuentran los catatumbos?',
    4 : '¿En cual país se encuentra la torre de Pisa?',
    5 : '¿Cual es la nación mas pequeña del mundo entero?',
    6 : '¿En cual país queda la famosa Torre Eiffel?',
    7 : '¿A cual ciudad se le conoce como  la ciudad de los rascacielos?',
    8 : '¿A que país pertenece la isla de Vieques?',
    9 : 'Es el lugar mas frío de la  tierra. Las capas de hielo que la cubren tienen un espesor entre dos mil y tres mil metros',
    10 : '¿Cual es la música típica de Argentina?'
};

var ENUM_OPCIONES_PREGUNTA_1 =['Moscu','Samara','San Petersburgo','Vladimir'];
var ENUM_OPCIONES_PREGUNTA_2 =['Kioto','Nagasaki','Osaka','Tokio'];
var ENUM_OPCIONES_PREGUNTA_3 =['Francia','Italia','Alemania','Holanda'];
var ENUM_OPCIONES_PREGUNTA_4 =['Italia','España','Portugal','Argentina'];
var ENUM_OPCIONES_PREGUNTA_5 =['El vaticano','Venecia','Barcelona','Montreal'];
var ENUM_OPCIONES_PREGUNTA_6 =['Paris','Viena','Londres','Madrid'];
var ENUM_OPCIONES_PREGUNTA_7 =['New York','Bogota','San Fransisco','Boston'];
var ENUM_OPCIONES_PREGUNTA_8 =['Puerto Rico','Panama','Costa Rica','Cuba'];
var ENUM_OPCIONES_PREGUNTA_9 =['Antartida','Argentina','Canada','Noruega'];
var ENUM_OPCIONES_PREGUNTA_10 =['Tango','Electronica','Paso Doble','Cumbia'];

var ENUM_RESPUESTAS =[ 'Moscu', 'Tokio', 'Francia', 'Italia', 'El vaticano', 'Paris', 'New York',  'Puerto Rico',  'Antartida', 'Tango' ];

var premio = 0;

var cantAciertos = 0;

var nuevoJuego = function (){
    var preguntaAleatoria = getPreguntaAleatoria();
    document.getElementById('pregunta').innerHTML = preguntaAleatoria;
    premio = 0;
    document.getElementById('premio_valor').innerHTML= premio;
    cantAciertos = 0;
};

var siguientePregunta = function (){
    var preguntaAleatoria = getPreguntaAleatoria();
    document.getElementById('pregunta').innerHTML = preguntaAleatoria;
};

var getPreguntaAleatoria = function () {
    var random = Math.random() * 10;
    var optNum = Math.ceil(random);
    getOrdenamientoOpciones(optNum);
    return ENUM_PREGUNTAS[optNum];
};

function getOrdenamientoOpciones(preguntaAleatoria){
    var lista =[0,1,2,3];
    if (preguntaAleatoria === 1){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_1[lista[i]];
        };
    }else if (preguntaAleatoria === 2){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_2[lista[i]];
        };
    }else if (preguntaAleatoria === 3){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_3[lista[i]];
        };
    }else if (preguntaAleatoria === 4){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_4[lista[i]];
        };
    }else if (preguntaAleatoria === 5){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_5[lista[i]];
        };
    }else if (preguntaAleatoria === 6){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_6[lista[i]];
        };
    }else if (preguntaAleatoria === 7){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_7[lista[i]];
        };
    }else if (preguntaAleatoria === 8){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_8[lista[i]];
        };
    }else if (preguntaAleatoria === 9){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_8[lista[i]];
        };
    }else if (preguntaAleatoria === 10){
        lista = lista.sort(function() {return Math.random() - 0.5});
        for (var i=0;i<4;i++){
            document.getElementById('R'.concat(i).toString()).innerHTML = ENUM_OPCIONES_PREGUNTA_10[lista[i]];
        };
    }


};

var revisa_si_correcta = function (respuestaSeleccionada){
    document.getElementById('respuestaSelect').innerHTML = document.getElementById(respuestaSeleccionada.toString()).innerHTML;
    for (var i=0;i<ENUM_RESPUESTAS.length;i++){
        if(document.getElementById(respuestaSeleccionada.toString()).innerHTML === ENUM_RESPUESTAS[i]){
            if (cantAciertos >= 0 && cantAciertos <=3){
                cantAciertos = cantAciertos + 1;
                premio = premio + 100;
                document.getElementById('premio_valor').innerHTML= premio;
                siguientePregunta();
                break;

            }else if (cantAciertos > 3 && cantAciertos <=5){
                cantAciertos = cantAciertos + 1;
                premio = premio + 1000;
                document.getElementById('premio_valor').innerHTML= premio;
                siguientePregunta();
                break;
            }else if (cantAciertos > 5 && cantAciertos <=10){
                cantAciertos = cantAciertos + 1;
                premio = premio + 10000;
                document.getElementById('premio_valor').innerHTML= premio;
                siguientePregunta();
                break;
            }

        }else{
           // alert('Ha perdido, su premio es: ' + premio );
           // nuevoJuego();
        }
    };


};

var retirarse = function(){
    alert("El su premio es:  $"+premio);
    nuevoJuego();
};